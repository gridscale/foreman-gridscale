# Changelog

## [v1.0.2]

- Allowed creating host to choose isoimage which available in gridscale user account
- Failover IPs are now correctly filtered out
- Targeting a correct URL to open VNC for a regular user

## [v1.0.1]

- Decresed dependency to foreman 1.15
- Missing URL field in `_gridscale.html.erb`
  This url field is needed for creating compute resource in foreman 1.15. 

## [v1.0.0] 

- Initial release of foreman-gridscale

